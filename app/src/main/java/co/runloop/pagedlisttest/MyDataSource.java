package co.runloop.pagedlisttest;

import android.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.paging.PositionalDataSource;

public class MyDataSource extends PositionalDataSource<Data> {

    private static final String TAG = MyDataSource.class.getSimpleName();

    private String name;
    private DataProvider dataProvider;

    public MyDataSource(String name) {
        this.name = name;
        Log.d(TAG, "name: " + "\"" + name + "\"");
        dataProvider = new DataProvider();
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<Data> callback) {
        int count = dataProvider.count(name);
        int startPos = computeInitialLoadPosition(params, count);
        int loadSize = computeInitialLoadSize(params, startPos, count);
        List<Data> data = dataProvider.loadData(name, startPos, loadSize);
        callback.onResult(data, startPos, count);
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<Data> callback) {
        List<Data> data = dataProvider.loadData(name, params.startPosition, params.loadSize);
        callback.onResult(data);
    }
}
