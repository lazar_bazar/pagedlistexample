package co.runloop.pagedlisttest;

import android.app.Application;

public class App extends Application {

    public static App get() {
        return instance;
    }

    private static volatile App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
