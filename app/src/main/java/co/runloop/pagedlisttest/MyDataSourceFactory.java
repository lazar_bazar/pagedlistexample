package co.runloop.pagedlisttest;

import androidx.annotation.NonNull;
import androidx.paging.DataSource;

public class MyDataSourceFactory extends DataSource.Factory<Integer, Data> {

    private String name;

    public MyDataSourceFactory(String name) {
        this.name = name;
    }

    @NonNull
    @Override
    public DataSource create() {
        return new MyDataSource(name);
    }

    public void setName(String name) {
        this.name = name;
    }
}
