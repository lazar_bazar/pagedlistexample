package co.runloop.pagedlisttest;

import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataProvider {

    public int count(String name) {
        Cursor cursor = App.get().getApplicationContext().getContentResolver()
                .query(ContactsContract.Contacts.CONTENT_URI,
                        new String[]{ContactsContract.Contacts._COUNT},
                        ContactsContract.Contacts.DISPLAY_NAME + " LIKE ?",
                        new String[]{"%" + name + "%"},
                        null);

        if (cursor != null) {
            try {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    return cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts._COUNT));
                }
            } finally {
                cursor.close();
            }
        }
        return 0;
    }

    public List<Data> loadData(String name, int offset, int limit) {
        Cursor cursor = App.get().getApplicationContext().getContentResolver()
                .query(ContactsContract.Contacts.CONTENT_URI,
                        null,
                        ContactsContract.Contacts.DISPLAY_NAME + " LIKE ?",
                        new String[]{"%" + name + "%"},
                        ContactsContract.Contacts.DISPLAY_NAME + " ASC LIMIT " + offset + ", " + limit);

        List<Data> data = null;
        if (cursor != null) {
            try {
                int count = cursor.getCount();
                if (count > 0) {
                    data = new ArrayList<>(count);
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        Data d = new Data();
                        d.setId(cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts._ID)));
                        d.setName(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                        data.add(d);
                        cursor.moveToNext();
                    }
                }
            } finally {
                cursor.close();
            }
        }

        if (data == null) {
            return Collections.emptyList();
        }
        return data;
    }
}
