package co.runloop.pagedlisttest;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class RvAdapter extends PagedListAdapter<Data, RvAdapter.MyViewHolder> {

    public RvAdapter(@NonNull DiffUtil.ItemCallback<Data> diffCallback) {
        super(diffCallback);
    }

    public RvAdapter(@NonNull AsyncDifferConfig<Data> config) {
        super(config);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recylcerview, parent, false);
        return new MyViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    @Override
    public void onViewRecycled(@NonNull MyViewHolder holder) {
        super.onViewRecycled(holder);
        holder.clear();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private Data data;
        private TextView tv;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.item_recyclerview_tv);
        }

        public void bind(Data data) {
            if (data == null) {
                tv.setText("Loading...");
                return;
            }
            this.data = data;
            tv.setText(data.getName());
        }

        public void clear() {
            data = null;
            tv.setText(null);
        }
    }
}
